trigger MovieTrigger on Movie__c (after update) {

    MovieTriggerHandler.handleAfterUpdate(Trigger.new);

}