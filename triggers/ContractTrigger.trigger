trigger ContractTrigger on Contract__c (before insert, after insert) {

    if (Trigger.isAfter && Trigger.isInsert){
        ContractTriggerHandler.handleAfterInsert(Trigger.new);
    } else
    if (Trigger.isBefore && Trigger.isInsert){
        ContractTriggerHandler.errors(Trigger.new);
    }

}