/**
 * Created by User on 21.12.2018.
 */

public with sharing class StudioVFPageController {

    public Studio__c studio { get; set; }
    public List<Movie__c> movieList { get; set; }
    puBlic Id movieId { get; set; }
    public Movie__c movieToUpdate { get; set; }

    public StudioVFPageController(ApexPages.StandardController std){
        studio = (Studio__c)std.getRecord();
        movieList = [SELECT Id, Name, Status__c, Studio__c FROM Movie__c WHERE Studio__c =: studio.Id AND Status__c = 'In process'];


    }

    public PageReference finished(){
        movieToUpdate = [SELECT Id, Name, Status__c FROM Movie__c WHERE Id =: movieId];
        movieToUpdate.Status__c = 'Finished';

        try {
            update movieToUpdate;
        } catch (Exception e){
            system.debug(e.getMessage());
        }
        movieList = [SELECT Id, Name, Status__c, Studio__c FROM Movie__c WHERE Studio__c =: studio.Id AND Status__c = 'In process'];
        return null;


    }

}