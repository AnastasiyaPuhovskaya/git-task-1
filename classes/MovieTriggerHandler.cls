/**
 * Created by User on 20.12.2018.
 */

public with sharing class MovieTriggerHandler {

    public static void handleAfterUpdate(List<Movie__c> movies){

        List<Payment__c> newPayments = new List<Payment__c>();
        List<Contract__c> allContracts = [SELECT Id, Movie__r.Id, Percent__c, RecordTypeId, Paid__c FROM Contract__c];

        Boolean bool = false;
        for (Movie__c movie : movies){
            if (movie.Status__c == 'Finished'){
                for (Integer i=0; i<allContracts.size(); i++){
                    if (movie.Id == allContracts[i].Movie__r.Id && movie.Gross__c != null){
                        if (allContracts[i].RecordTypeId ==  Schema.SObjectType.Contract__c.getRecordTypeInfosByName().get('Percent').getRecordTypeId() ||
                            allContracts[i].RecordTypeId ==  Schema.SObjectType.Contract__c.getRecordTypeInfosByName().get('Both').getRecordTypeId()){
                            Payment__c newPay = new Payment__c(
                                    Amount__c = (allContracts[i].Percent__c/100) * movie.Gross__c,
                                    Payment_Date__c = System.today(),
                                    Contract__c = allContracts[i].Id
                            );
                            newPayments.add(newPay);
                            allContracts[i].Paid__c = true;
                        }

                    }
                }
            }
        }
        insert newPayments;
        update allContracts;

    }

}