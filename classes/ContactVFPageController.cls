/**
 * Created by User on 21.12.2018.
 */

public with sharing class ContactVFPageController {

    public Contact contact { get; set; }
    public static List<Payment__c> payList { get; set; }
    public List<yearList> yearsList { get; set; }

    public ContactVFPageController(ApexPages.StandardController std){
        contact = (Contact)std.getRecord();
        payList = [SELECT Id, Payment_Date__c, Amount__c, Contract__r.Contact__c, Contract__r.Movie__r.Name, Contract__r.Movie__r.Studio__r.Name,
                   Contract__r.End_Date__c, Contract__r.Start_Date__c
                   FROM Payment__c WHERE Contract__r.Contact__c  =: contact.Id];

        Set<Integer> onlyYear = new Set<Integer>();
        for (Integer i=0; i<payList.size(); i++){
            Integer year = payList[i].Payment_Date__c.year();
            onlyYear.add(year);
        }

        List<Integer> sortedYear = new List<Integer>(onlyYear);
        sortedYear.sort();

        yearsList = new List<yearList>();
        for (Integer y : sortedYear){
            yearsList.add(new yearList(y));
        }


    public void methodB(){
        Integer i = 123;
    }

    public void methodA(){
        String str = 'a';

    }

        
    public class yearList{
        public Integer year { get; set;}
        public List<formForTable> yearInfo { get; set;}
        public Decimal totalAmount { get; set;}

        public yearList(Integer oneYear) {
            year = oneYear;
            totalAmount = 0;
            yearInfo = new List<ContactVFPageController.formForTable>();
            getYearInfo();

        }

        private void getYearInfo(){
            Set<Id> movieIdSet = new Set<Id>();

            for(Payment__c p : payList){
                if (movieIdSet.contains(p.Contract__r.Movie__c)){
                    for (formForTable f : yearInfo){
                        f.amount += p.Amount__c;
                        totalAmount += p.Amount__c;
                    }
                } else
                        if (p.Payment_Date__c.year() == year){
                            yearInfo.add(new formForTable(p));
                            movieIdSet.add(p.Contract__r.Movie__c);
                            totalAmount += p.Amount__c;
                }
            }
        }

    }


    public class formForTable{
        public String movie { get; set; }
        public String studio { get; set; }
        public Decimal amount { get; set; }
        public Integer duration { get; set; }
        public Id movieId { get; set; }

        public formForTable(Payment__c pay){
            this.movieId = pay.Contract__r.Movie__c;
            this.movie = pay.Contract__r.Movie__r.Name;
            this.studio = pay.Contract__r.Movie__r.Studio__r.Name;
            this.amount = pay.Amount__c;
            this.duration = pay.Contract__r.Start_Date__c.daysBetween(pay.Contract__r.End_Date__c);
        }
    }
}



