/**
 * Created by User on 20.12.2018.
 */

public with sharing class ContractTriggerHandler {

    public static void handleAfterInsert(List<Contract__c> contracts) {

        List <Contract__c> contrNew = new List <Contract__c>();
        for (Contract__c c : contracts) {
            contrNew.add(new Contract__c(Id = c.Id,  Paid__c = c.Paid__c, RecordTypeId = c.RecordTypeId, Amount__c = c.Amount__c));
        }



        List<Payment__c> newPayments = new List<Payment__c>();

        for (Contract__c contr : contrNew) {
            if (contr.RecordTypeId == Schema.SObjectType.Contract__c.getRecordTypeInfosByName().get('Both').getRecordTypeId() ||
                    contr.RecordTypeId == Schema.SObjectType.Contract__c.getRecordTypeInfosByName().get('Fixed').getRecordTypeId()) {
                Payment__c newPay = new Payment__c(
                        Amount__c = contr.Amount__c,
                        Payment_Date__c = System.Today(),
                        Contract__c = contr.Id
                );
                newPayments.add(newPay);
                if (contr.RecordTypeId == Schema.SObjectType.Contract__c.getRecordTypeInfosByName().get('Fixed').getRecordTypeId()) {
                    contr.Paid__c = true;
                } else {
                    contr.Paid__c = false;


                }

            }
        }
        insert newPayments;
        update contrNew;

    }


    public static void errors(List<Contract__c> contracts){

        List<Movie__c> allMovie = [SELECT Id, Status__c FROM Movie__c];

        for (Contract__c contr : contracts){
            for (Integer i=0; i<allMovie.size(); i++){
                if (contr.Movie__c == allMovie[i].Id){
                    if (allMovie[i].Status__c != 'New'){
                        contr.addError('Movie Status must be NEW');
                    }
                }
            }
        }

        List<Contract__c> allcontr = [SELECT Id, Start_Date__c, End_Date__c, Amount__c, Contact__r.Id, Movie__r.Id, Movie__r.Budget__c, Movie__r.Total_Contract_Amount__c FROM Contract__c];

        for (Contract__c contr : contracts){
            for (Integer i=0; i<allcontr.size(); i++){
                if (contr.Contact__c == allcontr[i].Contact__r.Id){
                    if ((contr.Start_Date__c >= allcontr[i].Start_Date__c && contr.Start_Date__c <= allcontr[i].End_Date__c) ||
                            (contr.End_Date__c >= allcontr[i].Start_Date__c && contr.End_Date__c <= allcontr[i].End_Date__c)){
                        contr.addError('Actor has a contract in this time!');
                    } else
                            if (contr.Movie__c == allcontr[i].Movie__r.Id){
                                contr.addError('Actor has a contract on this film!');
                            }
                }
                if (contr.Movie__c == allcontr[i].Movie__r.Id){
                    if (contr.Amount__c > allcontr[i].Movie__r.Budget__c - allcontr[i].Movie__r.Total_Contract_Amount__c){
                        contr.addError('Budget of this movie exceeded!');

                    }
                }

            }

        }




    }

}